#include <algorithm>
#include "ip_address.h"

static char* getCmdOption(char ** begin, 
	char ** end, const string & option)
{
	char ** itr = find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

static bool cmdOptionExists(char ** begin, 
	char ** end, const string& option)
{
	return find(begin, end, option) != end;
}

int main(int argc, char * argv[]) {		
	try {	
		// Check if first ip is greater than second
		if (cmdOptionExists(argv, argv + argc, "-gt")) {
			if (argc == 4) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				cout << (lhs > rhs);
			} else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Check if first ip is less than second
		if (cmdOptionExists(argv, argv + argc, "-lt")) {
			if (argc == 4) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				cout << (lhs < rhs);
			}
			else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Check if ips are equal
		if (cmdOptionExists(argv, argv + argc, "-eq")) {
			if (argc == 4) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				cout << (lhs == rhs);
			}
			else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Check if ips are not equal
		if (cmdOptionExists(argv, argv + argc, "-neq")) {
			if (argc == 4) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				cout << (lhs != rhs);
			}
			else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Check if ip is in range
		if (cmdOptionExists(argv, argv + argc, "-in")) {
			if (argc == 5) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				IpAddress needle(argv[4]);
				cout << (lhs <= needle && needle <= rhs);
			}
			else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Check if ip is not in range
		if (cmdOptionExists(argv, argv + argc, "-nin")) {
			if (argc == 5) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				IpAddress needle(argv[4]);
				cout << (needle < lhs || needle > rhs);
			}
			else {
				throw logic_error("Invalid arguments provided.");
			}
		}
		// Generate ip range
		if (cmdOptionExists(argv, argv + argc, "-range")) {
			if (argc == 4) {
				IpAddress lhs(argv[2]);
				IpAddress rhs(argv[3]);
				if (lhs < rhs) {
					IpAddress curr(lhs);
					while (curr <= rhs) {
						cout << curr.toString() << endl;
						curr = curr.next();
					}
				}				
			} else {
				throw logic_error("Invalid arguments provided.");
			}
		}
	} catch (exception e) {
		cout << "An exception occurred. " << e.what();
		return -1;
	}
	return 0;
}
