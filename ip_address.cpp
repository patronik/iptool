
#include "ip_address.h"

IpAddress::IpAddress(string src) {
	string elems[4];
	int i = 0;  
	for (char & c : src) {
		if (c == '.') {
			i++;
			continue;
		}
		elems[i] += c;
	}
	N0 = stoi(elems[0]);
	N1 = stoi(elems[1]);
	N2 = stoi(elems[2]);
	N3 = stoi(elems[3]);
}

IpAddress IpAddress::next() const {
	int n0 = getN0(), n1 = getN1(),
		n2 = getN2(), n3 = getN3();
	if (n3 < 255) {
		return IpAddress(n0, n1, n2, n3 + 1);
	} else {
		if (n2 < 255) {
			return IpAddress(n0, n1, n2 + 1, 0);
		} else {
			if (n1 < 255) {
				return IpAddress(n0, n1 + 1, 0, 0);
			} else {
				if (n0 < 255) {
					return IpAddress(n0 + 1, 0, 0, 0);
				} else {
					return *this;
				}
			}
		}
	}
}

IpAddress IpAddress::prev() const {
	int n0 = getN0(), n1 = getN1(),
		n2 = getN2(), n3 = getN3();
	if (n3 > 0) {
		return IpAddress(n0, n1, n2, n3 - 1);
	} else {
		if (n2 > 0) {
			return IpAddress(n0, n1, n2 - 1, 255);
		} else {
			if (n1 > 0) {
				return IpAddress(n0, n1 - 1, 255, 255);
			} else {
				if (n0 > 0) {
					return IpAddress(n0 - 1, 255, 255, 255);
				} else {
					return *this;
				}
			}
		}
	}
}

bool IpAddress::operator>=(IpAddress & rhs) const
{	
	if (operator>(rhs) || operator==(rhs)) {
		return true;
	}
	return false;
}

bool IpAddress::operator<=(IpAddress & rhs) const
{	
	if (operator<(rhs) || operator==(rhs)) {
		return true;
	}
	return false;
}

bool IpAddress::operator>(IpAddress & rhs) const
{		
	// N0
	if (N0 > rhs.getN0()) {
		return true;
	}
	if (N0 < rhs.getN0()) {
		return false;
	}
	// N1
	if (N1 > rhs.getN1()) {
		return true;
	}
	if (N1 < rhs.getN1()) {
		return false;
	}
	// N2
	if (N2 > rhs.getN2()) {
		return true;
	}
	if (N2 < rhs.getN2()) {
		return false;
	}
	// N3
	if (N3 > rhs.getN3()) {
		return true;
	}
	if (N3 < rhs.getN3()) {
		return false;
	}
	return false;
}

bool IpAddress::operator<(IpAddress & rhs) const
{		
	// N0
	if (N0 < rhs.getN0()) {
		return true;
	}
	if (N0 > rhs.getN0()) {
		return false;
	}
	// N1
	if (N1 < rhs.getN1()) {
		return true;
	}
	if (N1 > rhs.getN1()) {
		return false;
	}
	// N2
	if (N2 < rhs.getN2()) {
		return true;
	}
	if (N2 > rhs.getN2()) {
		return false;
	}
	// N3
	if (N3 < rhs.getN3()) {
		return true;
	}
	if (N3 > rhs.getN3()) {
		return false;
	}
	return false;
}

bool IpAddress::operator==(IpAddress & rhs) const
{	
	if ((N0 == rhs.getN0()) 
		&& (N1 == rhs.getN1())
		&& (N2 == rhs.getN2()) 
		&& (N3 == rhs.getN3())) {
		return true;
	}
	return false;
}

