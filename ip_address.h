
#ifndef ip_address_h
#define ip_address_h

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class IpAddress {
    
	short int N0;
	short int N1;
	short int N2;
	short int N3;

public:
	
	IpAddress(string src);

	IpAddress(const char * src) :
		IpAddress(string(src)) {}

	IpAddress(int n0, int n1, int n2, int n3) :
		N0(n0), N1(n1), N2(n2), N3(n3) {}

	bool isValid() const {
		if ((N0 >= 0 && N0 <= 255) 
			&& (N1 >= 0 && N1 <= 255)
			&& (N2 >= 0 && N2 <= 255) 
			&& (N3 >= 0 && N3 <= 255))
		{
			return true;
		}
		return false;
	}

	string toString() const {
		return to_string(N0) + string(".") + to_string(N1) 
			+ string(".") + to_string(N2) + string(".") + to_string(N3);
	}

	short int getN0() const { return N0; }

	short int getN1() const { return N1; }

	short int getN2() const { return N2; }

	short int getN3() const { return N3; }

	bool operator==(IpAddress & rhs) const;

	bool operator!=(IpAddress & rhs) const {
		return !operator==(rhs);
	};

	bool operator<(IpAddress & rhs) const;

	bool operator>(IpAddress & rhs) const;

	bool operator<=(IpAddress & rhs) const;

	bool operator>=(IpAddress & rhs) const;

	IpAddress next() const;

	IpAddress prev() const;	
};

#endif

